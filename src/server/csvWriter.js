const fs = require('fs');
const path = require('path');
const csv = require('csv-parser');
const ipl = require('./ipl');

/**
 * Asynchronously parse a csv file.
 * @param {String} filePath Path to the csv file.
 * @returns {Promise} Promise object represents the Array of csv data
 */
function readCsvFile(filePath) {
  return new Promise((resolve, reject) => {
    const allData = [];

    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (data) => {
        allData.push(data);
      })
      .on('end', () => resolve(allData));
  });
}

/**
 * Writes data to a JSON file.
 * Data is JSON.stringified before writing.
 * @param {Object} data The data in Object form to write in the JSON file
 * @param {String} fullPath Full path of the file where the data is to be written including the filename.
 * Filename must end in '.json'. If filename is not provided, defaults to 'data.json'.
 */
function writeToJsonFile(data, fullPath) {
  const filePath = path.parse(fullPath);
  data = JSON.stringify(data);

  if (filePath.ext != '.json') {
    filePath.base = 'data.json';
    filePath.ext = '.json';
  }

  fs.mkdir(filePath.dir, { recursive: true }, (mkdirError) => {
    if (mkdirError) throw mkdirError;
    else {
      fs.writeFile(path.join(filePath.dir, filePath.base), data, (writeFileError) => {
        if (writeFileError) throw writeFileError;
      });
    }
  });
}

/**
 * Reads the matches data from csv file and writes matches played per year to json file.
 * @param {String} matchesCsvPath Path to the matches csv file.
 * @param {String} outputJsonPath Path of the output JSON file including filename.
 */
function writeMatchesPlayedPerYear(matchesCsvPath, outputJsonPath) {
  readCsvFile(matchesCsvPath)
    .then((allMatches) => {
      const jsonData = ipl.matchesPlayedPerYear(allMatches);
      writeToJsonFile(jsonData, outputJsonPath);
    })
    .catch((err) => console.error(err));
}

/**
 * Reads the matches data from csv file
 * then writes matches played per year per team to JSON file.
 * @param {String} matchesCsvPath Path to the matches csv file.
 * @param {String} outputJsonPath Path of the output JSON file including filename.
 */
function writeMatchesWonPerTeamPerYear(matchesCsvPath, outputJsonPath) {
  readCsvFile(matchesCsvPath)
    .then((allMatches) => {
      const jsonData = ipl.matchesWonPerTeamPerYear(allMatches);
      writeToJsonFile(jsonData, outputJsonPath);
    })
    .catch((err) => console.error(err));
}

/**
 * Reads the data from matches and deliveries csv file
 * then writes the number of extra runs conceded per team
 * for the specified year to json file.
 * @param {String} matchesCsvPath Path to the matches csv file.
 * @param {String} deliveriesCsvPath Path to the deliveries csv file.
 * @param {String} outputJsonPath Path of the output JSON file including filename.
 */
function writeExtraRunsConcededPerTeamForYear(matchesCsvPath, deliveriesCsvPath, outputJsonPath) {
  let allMatches;
  let allDeliveries;

  readCsvFile(matchesCsvPath)
    .then((matchesData) => {
      allMatches = matchesData;
      return readCsvFile(deliveriesCsvPath);
    })
    .then((deliveriesData) => {
      allDeliveries = deliveriesData;
    })
    .then(() => {
      const jsonData = ipl.extraRunsConcededPerTeamForYear(allMatches, allDeliveries, 2016);
      writeToJsonFile(jsonData, outputJsonPath);
    })
    .catch((err) => console.error(err));
}

/**
 * Reads the data from matches and deliveries csv file.
 * then writes the specified number of top economical bowlers
 * with their economy rate to json file.
 * @param {String} matchesCsvPath Path to the matches csv file.
 * @param {String} deliveriesCsvPath Path to the deliveries csv file.
 * @param {String} outputJsonPath Path of the output JSON file including filename.
 */
function writeTopEconomicalBowlersForYear(matchesCsvPath, deliveriesCsvPath, outputJsonPath) {
  let allMatches;
  let allDeliveries;

  readCsvFile(matchesCsvPath)
    .then((matchesData) => {
      allMatches = matchesData;
      return readCsvFile(deliveriesCsvPath);
    })
    .then((deliveriesData) => {
      allDeliveries = deliveriesData;
    })
    .then(() => {
      const jsonData = ipl.topEconomicalBowlersForYear(allMatches, allDeliveries, 2015, 10);
      writeToJsonFile(jsonData, outputJsonPath);
    })
    .catch((err) => console.error(err));
}

module.exports = {
  writeMatchesPlayedPerYear,
  writeMatchesWonPerTeamPerYear,
  writeExtraRunsConcededPerTeamForYear,
  writeTopEconomicalBowlersForYear,
};
