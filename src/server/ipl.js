const fs = require("fs");
const path = require("path");

/**
 * Returns the number of matches played per year for all the years in the dataset.
 * @param {Array.<Object>} allMatches Array of all the matches.
 * Each match should be an {object} and have {season} property.
 * @returns {Object.<String, Number>} Year and count as key, value pair.
 */
function matchesPlayedPerYear(allMatches) {
  const result = {};

  for (const match of allMatches) {
    const season = match.season;

    if (result.hasOwnProperty(season)) {
      result[season] += 1;
    } else {
      result[season] = 1;
    }
  }

  return result;
}

/**
 * Returns the number of matches each team won per year.
 * @param {Array.<Object>} allMatches Array of all the matches.
 * Each match should be an {object} and have {season} and {winner} property
 * @returns {Object} Number of matches each team won in all years. Below is an example structure of the return object.
 * @example
 * {
 *   "Royal Challengers Bangalore": {
 *     "2008":4,
 *     "2009":9,
 *     "2010":8,
 *   },
 *   "Rising Pune Supergiants": {
 *     "2016":4,
 *   },
 * }
 */
function matchesWonPerTeamPerYear(allMatches) {
  let result = {};

  for (const match of allMatches) {
    const winner = match.winner;
    const year = match.season;

    if (winner.length === 0) continue;

    if (result.hasOwnProperty(winner)) {
      if (result[winner].hasOwnProperty(year)) {
        result[winner][year] += 1;
      } else {
        result[winner][year] = 1;
      }
    } else {
      result[winner] = {};
    }
  }

  return result;
}

/**
 * Returns the number of extra runs each team conceded for a particular year.
 * @param {Array.<Object>} allMatches Array of all the matches.
 * Each match should be an {object} and have {season} and {id} property
 * @param {Array.<Object>} allDeliveries Array of all the deliveries.
 * Each delivery should be an {object} and have {extra_runs}, {batting_team} and {match_id} property.
 * @param {Number} [year=2016] The year for which to calculate the extra runs.
 * @returns {Object.<String, Number>} Team and extra runs as key, value pair.
 */
function extraRunsConcededPerTeamForYear(allMatches, allDeliveries, year = 2016) {
  const matchesForYear = [];
  const result = {};

  for (const match of allMatches) {
    if (match.season == year) matchesForYear.push(match.id);
  }

  for (const delivery of allDeliveries) {
    if (matchesForYear.includes(delivery.match_id)) {
      const extraRuns = parseInt(delivery.extra_runs);

      if (result.hasOwnProperty(delivery.batting_team)) {
        result[delivery.batting_team] += extraRuns;
      } else {
        result[delivery.batting_team] = extraRuns;
      }
    }
  }

  return result;
}

/**
 * Calculates and Returns the top economical bowlers from the given data.
 * @param {Array.<Object>} allMatches Array of all the matches.
 * Each match should be an {object} and have {season} and {id} property
 * @param {Array.<Object>} allDeliveries Array of all the deliveries.
 * Each delivery should be an {object} and have {extra_runs}, {batting_team} and {match_id} property.
 * @param {Number} [year=2015] The year for which to calculate the extra runs.
 * @param {Number} [limit=10] The number of bowlers to limit in the output.
 * @returns {Object.<String, Number>} Top Bowlers with their economy rate as key, value pair.
 * Data is sorted in ascending order of economy rate.
 */
function topEconomicalBowlersForYear(allMatches, allDeliveries, year = 2015, limit = 10) {
  const matchesForYear = [];
  const bowlerData = {};
  const topBowlers = [];

  for (const match of allMatches) {
    if (match.season == year) matchesForYear.push(match.id);
  }

  for (const delivery of allDeliveries) {
    if (matchesForYear.includes(delivery.match_id)) {
      if (bowlerData.hasOwnProperty(delivery.bowler)) {
        bowlerData[delivery.bowler].balls += 1;
        bowlerData[delivery.bowler].runs += parseInt(delivery.total_runs);
      } else {
        bowlerData[delivery.bowler] = {
          balls: 1,
          runs: parseInt(delivery.total_runs),
        };
      }
    }
  }

  for (const bowler in bowlerData) {
    const economyRate = bowlerData[bowler].runs / (bowlerData[bowler].balls / 6);
    topBowlers.push({ name: bowler, economy_rate: parseFloat(economyRate.toFixed(2)) });
  }

  topBowlers.sort((firstBowler, secondBowler) => {
    if (firstBowler.economy_rate > secondBowler.economy_rate) {
      return 1;
    } else if (firstBowler.economy_rate < secondBowler.economy_rate) {
      return -1;
    } else {
      return 0;
    }
  });

  if (topBowlers.length > limit) {
    return topBowlers.splice(0, limit);
  }

  return topBowlers;
}

module.exports = {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamForYear,
  topEconomicalBowlersForYear,
};
