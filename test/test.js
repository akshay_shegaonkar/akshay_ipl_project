const fs = require('fs');
const csv = require('csv-parser');

const {
  writeMatchesPlayedPerYear,
  writeMatchesWonPerTeamPerYear,
  writeExtraRunsConcededPerTeamForYear,
  writeTopEconomicalBowlersForYear,
} = require('../src/server/csvWriter');

const DELIVERIES = './src/data/deliveries.csv';
const MATCHES = './src/data/matches.csv';

writeMatchesPlayedPerYear(MATCHES, './src/public/output/matchesPlayedPerYear.json');
// console.log("writing matchesPlayedPerYear.json complete");

writeMatchesWonPerTeamPerYear(MATCHES, './src/public/output/matchesWonPerTeamPerYear.json');
// console.log("writing matchesWonPerTeamPerYear.json complete");

writeExtraRunsConcededPerTeamForYear(
  MATCHES,
  DELIVERIES,
  './src/public/output/extraRunsConcededPerTeamForYear.json'
);
// console.log("writing extraRunsConcededPerTeamForYear.json complete");

writeTopEconomicalBowlersForYear(
  MATCHES,
  DELIVERIES,
  './src/public/output/topEconomicalBowlersForYear.json'
);
// console.log("writing topEconomicalBowlersForYear.json complete");
